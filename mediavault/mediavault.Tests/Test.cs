﻿using NUnit.Framework;
using System;

namespace MediaVault.Tests
{
	[TestFixture]
	public class Test
	{
		#region for SignIn
		[Test]
		public void SignInEmptyUsername()
		{
			SignIn si = new SignIn("", "xxx");
			Assert.True(si.feedback == "Username or Password is empty");
		}

		[Test]
		public void SignInEmptyPassword()
		{
			SignIn si = new SignIn("xxx", "");
			Assert.True(si.feedback == "Username or Password is empty");
		}

		[Test]
		public void SignInEmptyUsernameorPassword()
		{
			SignIn si = new SignIn("", "");
			Assert.True(si.feedback == "Username or Password is empty");
		}

		[Test]
		public void SignInUIDIsZero()
		{
			SignIn si = new SignIn("xxx", "xxx");
			Assert.True(si.feedback == "Username or Password does not exist");
		}
		#endregion

		#region sign up
		[Test]
		public void SignUpEmptyField()
		{
			SignUp su = new SignUp("", "", "", "");
			Assert.True(su.feedback == "Email or Username or Password is empty");
		}

		[Test]
		public void SignUpPasswordMatch()
		{
			SignUp su = new SignUp("xxx", "xxx", "admin", "admins");
			Assert.True(su.feedback == "Password did not match");
		}
		#endregion

	}
}
