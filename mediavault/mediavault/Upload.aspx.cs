using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Web.Configuration;

namespace MediaVault
{


	public partial class Upload : System.Web.UI.Page
	{
		public const int LIMIT = 25;
		private List<string> phototypes = new List<string> { "image/jpeg", "image/png" };
		private List<string> audiotypes = new List<string> { "audio/wav", "audio/mpeg" };
		private List<string> filetypes = new List<string> { };

		protected void UploadButtonClicked(object sender, EventArgs args)
		{
			filetypes.AddRange(phototypes);
			filetypes.AddRange(audiotypes);

			if (fileuploadcontrol.HasFile)
			{
				try
				{
					if (filetypes.Contains(fileuploadcontrol.PostedFile.ContentType))
					{
						if (fileuploadcontrol.PostedFile.ContentLength < GetSizeLimit() * LIMIT)
						{
							statuslabel.Text = "Upload status: File uploading...";
							string filename = Path.GetFileName(fileuploadcontrol.FileName);
							var location = GetLocation(fileuploadcontrol.PostedFile.ContentType);
							fileuploadcontrol.SaveAs(location + filename);
							statuslabel.Text = "Upload status: File uploaded!";
							int size = GetSizeLimit();
							var content = fileuploadcontrol.PostedFile.ContentType;
							GenerateDetails(location + filename);
							fileuploadcontrol.Dispose();
						}
						else
						{
							statuslabel.Text = "Upload status: File size Limit exceeded! Should be less than 25MB";
						}
					}
					else
					{
						statuslabel.Text = "Upload status: Only wav, mp3, jpeg and png files are accepted!";
					}
				}
				catch (Exception ex)
				{
					statuslabel.Text = "Upload status: The file cound not be uploaded. " + ex.Message;
				}
			}
		}

		private int GetSizeLimit()
		{
			int maxRequestLength = 0;
			HttpRuntimeSection section = WebConfigurationManager.GetSection("system.web/httpRuntime") as HttpRuntimeSection;
			try
			{
				if (section != null)
				{
					maxRequestLength = section.MaxRequestLength;
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Unexpected error when extracting file size limit" + ex.Message);
			}
			return maxRequestLength;
		}


		private string GetLocation(string contenttype)
		{
			var location = Server.MapPath("~/App_Data/");
			if (audiotypes.Contains(contenttype))
			{
				location += "Audio/";
			}
			else if (phototypes.Contains(contenttype))
			{
				location += "Photo/";
			}
			return location;
		}

		private void GenerateDetails(string path)
		{
			FileInfo newinfo = new FileInfo(path);

			long userid = Default.GetUseridLoggedInUser(); //user id;
			string name = newinfo.Name; //name;
			long size = newinfo.Length; //size;
			string location = "~/App_Data/"; //location
			TagLib.File meta = TagLib.File.Create(path);
			Models.Metadata metadata = GetFields(meta, userid, newinfo);
			switch (newinfo.Directory.Name)
			{
				case "Photo":
					Models.Photo newphoto = new Models.Photo();
					newphoto.PID = 0;
					newphoto.UID = userid;
					newphoto.photo_name = name;
					newphoto.size = size;
					newphoto.photo_path = location+"Photo/"+name;
					newphoto.Metadata = metadata;
					newphoto.MID = MediaVault.DatabaseLayer.MetadataTable.AddMetadata(metadata);//parse items to table metadata
					if (newphoto.MID == 0)
					{
						statuslabel.Text = "Upload status: Metadata Parsing error to database";
						return;
					}
					//parse items to table photo
					if (MediaVault.DatabaseLayer.PhotoTable.AddPhoto(newphoto) == 0)
					{
						statuslabel.Text = "Upload status: Photo Parsing error to database";
						return;
					}
					break;
				case "Audio":
					Models.Audio newaudio = new Models.Audio();
					newaudio.AID = 0;
					newaudio.UID = userid;
					newaudio.audio_name = name;
					newaudio.size = size;
					newaudio.audio_path = location+"Audio/"+name;
					newaudio.duration = meta.Properties.Duration;
					newaudio.samplerate = meta.Properties.AudioSampleRate;
					newaudio.channels = meta.Properties.AudioChannels;
					newaudio.Metadata = metadata;
					newaudio.MID = MediaVault.DatabaseLayer.MetadataTable.AddMetadata(metadata);//parse items to table metadata
					if (newaudio.MID == 0)
					{
						statuslabel.Text = "Upload status: Metadata Parsing error to database";
						return;
					}
					if (MediaVault.DatabaseLayer.AudioTable.AddAudio(newaudio) == 0)
					{
						statuslabel.Text = "Upload status: Audio Parsing error to database";
						return;
					}
					break;
				case "Video":
					break;
				case "Ebook":
					break;
			}

		}


		private Models.Metadata GetFields(TagLib.File meta, long userid, FileInfo info)
		{
			Models.Metadata metadata = new Models.Metadata();
			metadata.MID = 0;
			metadata.UID = userid;
			switch (info.Directory.Name)
			{
				case "Photo":
					metadata.dimension_height = meta.Properties.PhotoHeight;
					metadata.dimension_width = meta.Properties.PhotoWidth;
					break;
				case "Audio":
					metadata.dimension_width = 0;
					metadata.dimension_width = 0;
					break;
				case "Video":
					metadata.dimension_width = meta.Properties.VideoWidth;
					metadata.dimension_height = meta.Properties.VideoHeight;
					break;
				case "Ebook"://this needs to be changed may be
					break;
			}
			metadata.type = info.Extension;
			metadata.created_date = info.CreationTime;
			metadata.modified_date = info.CreationTime;
			metadata.author = string.Join(",", meta.Tag.AlbumArtists);
			metadata.album = meta.Tag.Album;
			metadata.genre = string.Join(",", meta.Tag.Genres);
			metadata.title = meta.Tag.Title;

			return metadata;
		}

	}
}
