﻿using System;
using MySql.Data.MySqlClient;

namespace MediaVault
{
	/// <summary>
	/// This class is responsible for facilitating signing in process to media vault.
	/// </summary>
	public class SignIn
	{
		private string username;
		private string password;
		public long userid { set; get;  }
        public string feedback { get; }

		public SignIn(string username=null, string password=null)
		{
            this.feedback = "";
			if (username.Length == 0 || password.Length == 0)
			{
				this.feedback = "Username or Password is empty";
			}
            else if ((this.userid = GetUID(username, password)) == 0)
			{
				this.feedback = "Username or Password does not exist";
			}
			this.username = username;
			this.password = password;
		}

		private static long GetUID(string username,string password)
		{
			string sql = @"SELECT id, password, name FROM user";
			DatabaseClass dlink = new DatabaseClass();
			MySqlCommand cmd = dlink.conn.CreateCommand();
			cmd.CommandText = (sql);
			cmd.Connection = dlink.conn;
			MySqlDataReader reader = cmd.ExecuteReader();
			while (reader.Read())
			{
				if (reader.GetString(1).ToString() == password && reader.GetString(2).ToString() == username)
				{
					long id = long.Parse(reader.GetString(0).ToString());
					cmd.Connection.Close();
					return id;
				}
			}
			cmd.Connection.Close();
			return 0; // user doesn't exists
		}
	}
}
