﻿using System;
using MySql.Data.MySqlClient;

namespace MediaVault
{
    public class DatabaseClass
    {
        public const string server = "localhost";
        public const string database = "mediavault";
        public const string username = "root";
        public const string password = "group33";

        public MySqlConnection conn { get; }
        public string myConnectionString { get; }

        public DatabaseClass()
        {
            myConnectionString = "server=" + server + ";uid=" + username + ";pwd=" + password + ";database=" + database + ";";
			try
			{
				conn = new MySqlConnection();
				conn.ConnectionString = myConnectionString;
				conn.Open();
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
        }
    }
}
