﻿using System;
namespace MediaVault
{
	public static class MVException
	{
		public static void EmptyField(string field)
		{
			throw new Exception(field + " is empty");
		}

		public static void NotValidField(string field)
		{
			throw new Exception(field + " does not exists");
		}

		public static void FieldExists(string field)
		{
			throw new Exception("provided " + field + " already exists");
		}

		public static void Unexpected()
		{
			throw new Exception("Prior to this other method needs to be called");
		}
	}
}
