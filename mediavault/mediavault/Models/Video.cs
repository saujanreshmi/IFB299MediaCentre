﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MediaVault.Models
{
	public class Video
	{
		public Video()
		{
		}
		[ScaffoldColumn(false)]
		public long VID { get; set; }

		public long UID { get; set; }

		public long MID { get; set; }

		[Required, StringLength(50), Display(Name = "Name")]
		public string video_name { get; set; }

		[ScaffoldColumn(false)]
		public long size { get; set; }

		[ScaffoldColumn(false)]
		public string video_path { get; set; }

		[ScaffoldColumn(false)]
		public float duration { get; set; }

		[ScaffoldColumn(false)]
		public long samplerate { get; set; }

		[ScaffoldColumn(false)]
		public long channels { get; set; }

		public virtual Metadata Metadata { get; set; }
	}
}
