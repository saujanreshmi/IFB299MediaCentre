﻿using System.ComponentModel.DataAnnotations;
using System;

namespace MediaVault.Models
{
    public class Metadata
    {
		public Metadata() 
		{ 
		}

		public const int Column = 11;

		public static string[] Attributes = new string[11] { "MID", "UID", "dimension_height", "dimension_width", "type", "created_date","modified_date","author","album","genre","title"};

        [ScaffoldColumn(false)]
        public long MID { get; set; }

        public long UID { get; set; }

        [ScaffoldColumn(false)]
        public long dimension_height { get; set; }

        [ScaffoldColumn(false)]
        public long dimension_width { get; set; }

        [ScaffoldColumn(false)]
        public string type { get; set; }

        [ScaffoldColumn(false)]
        public DateTime created_date { get; set; }

        [ScaffoldColumn(false)]
        public DateTime modified_date {get; set;}

        [ScaffoldColumn(true)]
        public string author { get; set; }

        [ScaffoldColumn(true)]
        public string album { get; set; }

        [ScaffoldColumn(true)]
        public string genre { get; set; }

        [ScaffoldColumn(true)]
        public string title { get; set; }
    }
}
