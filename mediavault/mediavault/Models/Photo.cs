﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MediaVault.Models
{
	public class Photo
	{
		public const int Column = 6;

		public static string[] Attributes = new string[6] {"PID","UID","photo_name","MID","size","photo_path"};

		public long PID { get; set; }

		public long UID { get; set; }

		public string photo_name { get; set; }
		 
		public long MID { get; set; }

		public long size { get; set; }

		public string photo_path { get; set; }

		public virtual Metadata Metadata { get; set; }
	}
}
