﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace MediaVault
{

	public partial class Photo : System.Web.UI.Page
	{
		DatabaseLayer.PhotoTable ph;
		Dictionary<long, Models.Photo> p; 

		private void Page_Load(object sender, System.EventArgs e)
		{
			ph = new DatabaseLayer.PhotoTable();
			p = ph.GetPhotos(Default.GetUseridLoggedInUser());
			int i = 0;
			foreach (var item in p)
			{
				switch (i)
				{
					case 0:
						firstquarter.InnerHtml += "<img src='" + item.Value.photo_path + "' style= 'width:100%' alt='" + item.Value.photo_name + "'onclick='onClick(this)' />";
						i = 1;
						break;
					case 1:
						secondquarter.InnerHtml += "<img src='" + item.Value.photo_path + "' style= 'width:100%' alt='" + item.Value.photo_name + "'onclick='onClick(this)' />";
						i = 2;
						break;
					case 2:
						thirdquarter.InnerHtml += "<img src='" + item.Value.photo_path + "' style= 'width:100%' alt='" + item.Value.photo_name + "' onclick='onClick(this)' />";
						i = 3;
						break;
					case 3:
						fourthquarter.InnerHtml += "<img src='" + item.Value.photo_path + "' style= 'width:100%' alt='" + item.Value.photo_name + "' onclick='onClick(this)' />";
						i = 0;
						break;
				}
			}
		}


		public void CheckbuttonClicked(object sender, EventArgs args)
		{
			
		}
	}
}
