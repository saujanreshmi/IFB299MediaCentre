﻿using System;
using System.Web;
using System.Web.UI;
using System.Drawing;

namespace MediaVault
{

	public partial class Register : System.Web.UI.Page
	{
		public void RegisterClicked(object sender, EventArgs args)
		{
			SignUp s2 = new SignUp(signupemailtextbox.Text, signupusernametextbox.Text, signuppasswordtextbox.Text, signupretypepasswordtextbox.Text);
			if (s2.feedback.Length != 0)
			{
				signupusernametextbox.Text = string.Empty;
				signupemailtextbox.Text = string.Empty;
				signupmessage.Text = s2.feedback;
			}
			else
			{
				if (s2.FillDatbase())
				{
					signupusernametextbox.Text = string.Empty;
					signupemailtextbox.Text = string.Empty;
					accountmessage.Text = "Account Created!";
				}
				else
				{
					signupmessage.Text = "Account is not Created!!";
				}
			}
		}
	}
}
