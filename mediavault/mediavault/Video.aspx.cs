﻿using System;
using System.Web;
using System.Web.UI;

namespace MediaVault
{

	public partial class Video : System.Web.UI.Page
	{
		public void PhotoClicked(object sender, EventArgs args)
		{
			Response.Redirect("Photo.aspx");
		}

		public void VideoClicked(object sender, EventArgs args)
		{
			Response.Redirect("Video.aspx");
		}

		public void EbookClicked(object sender, EventArgs args)
		{
			Response.Redirect("Ebook.aspx");
		}

		public void AudioClicked(object sender, EventArgs args)
		{
			Response.Redirect("Audio.aspx");
		}

		public void UploadClicked(object sender, EventArgs args)
		{
			Response.Redirect("Upload.aspx");
		}
	}
}
