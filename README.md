# IFB299MediaCentre
# This is Media vault project repository for Group Thirty three.

All the documentation files are in Documentation folder 

Unit test case in: IFB299MediaCenter > mediavault > mediavault.Tests

Website file systems in: IFB299MediaCenter > mediavault 

Database script for constructing database schema for this project in: IFB299MediaCenter > Databasescript

Docker-compose files that runs seprate machine for database in: IFB299MediaCenter > database-container
