﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MediaVault.Models
{
	public class Ebook
	{
		public Ebook()
		{
		}

		[ScaffoldColumn(false)]
		public long EID { get; set; }

		[ScaffoldColumn(false)]
		public long UID { get; set; }

		[Required, StringLength(50), Display(Name = "Name")]
		public string ebook_name { get; set; }

		[ScaffoldColumn(false)]
		public long MID { get; set; }

		[ScaffoldColumn(false)]
		public long size { get; set; }

		[ScaffoldColumn(false)]
		public string ebookpath { get; set; }

		[ScaffoldColumn(false)]
		public long pages { get; set; }

		public virtual Metadata Metadata { get; set; }
	}
}
