﻿using System;
using MediaVault.Models;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace MediaVault.DatabaseLayer
{
    public class PhotoTable
    {

		//Datatype to keep track of photos for specific user
		private Dictionary<long, Models.Photo> photos;

		public PhotoTable()
		{
			this.photos = new Dictionary<long, Models.Photo>();
		}

		public Dictionary<long, Models.Photo> GetPhotos(long user_id)
		{
			
			SkimDatabase(user_id);
			return this.photos;
		}

        public static long AddPhoto(Models.Photo newphoto)
        {
			long photo_id = FillDatabase(newphoto);

			return photo_id;
        }


		private static long FillDatabase(Models.Photo newphoto)
		{
			string sql = @"INSERT INTO photo(id, u_id, name, m_id, size, location) VALUES(@id, @u_id, @name, @m_id, @size, @location)";
			DatabaseClass dlink = new DatabaseClass();
			MySqlCommand cmd = dlink.conn.CreateCommand();
			cmd.CommandText = sql;
			if (newphoto != null)
			{
				if (newphoto.UID < 0 && newphoto.photo_name == null && newphoto.MID < 0 && newphoto.size < 0 && newphoto.photo_path == null && newphoto.Metadata == null)
				{
					return 0;
				}
				else
				{
					cmd.Parameters.AddWithValue("@id", newphoto.PID);
					cmd.Parameters.AddWithValue("@u_id", newphoto.UID);
					cmd.Parameters.AddWithValue("@name", newphoto.photo_name);
					cmd.Parameters.AddWithValue("@m_id", newphoto.MID);
					cmd.Parameters.AddWithValue("@size", newphoto.size);
					cmd.Parameters.AddWithValue("@location", newphoto.photo_path);
					if (cmd.ExecuteNonQuery() == 1)
					{
						return cmd.LastInsertedId;
					}
				}
			}
			return 0; //not successful
		}

        public void SkimDatabase(long user_id)
        {
			//TODO loop through photos in database
			DatabaseClass dlink = new DatabaseClass();
			MySqlCommand cmd = dlink.conn.CreateCommand();
			cmd.CommandText = @"SELECT * FROM photo WHERE u_id="+user_id.ToString();
			cmd.Connection = dlink.conn;
			MySqlDataReader reader = cmd.ExecuteReader();
			while (reader.Read())
            {
				//TODO
				Dictionary<string, string> p = new Dictionary<string, string>();
				for (int i = 0; i < Models.Photo.Column; i++)
				{
					string value = reader.IsDBNull(i) ? "" : reader.GetString(i);
					p.Add(Models.Photo.Attributes[i], value);
				}
				//add in photo dictionary
				this.photos.Add(this.photos.Count+1, CreatePhotoObject(p, user_id));
            }
			
        }


        private static Models.Photo CreatePhotoObject(Dictionary<string, string> dbphoto, long user_id)
        {
            Models.Photo p = new Models.Photo();
			foreach (var item in dbphoto)
			{
				switch (item.Key)
				{
					case "PID":
						p.PID = long.Parse(item.Value);
						break;
					case "UID":
						p.UID = long.Parse(item.Value);
						break;
					case "photo_name":
						p.photo_name = item.Value;
						break;
					case "MID":
						p.MID = long.Parse(item.Value);
						break;
					case "size":
						p.size = long.Parse(item.Value);
						break;
					case "photo_path":
						p.photo_path = item.Value;
						break;
				}
			}
			p.Metadata = MetadataTable.GetMetadata(user_id, p.MID);
            return p;
        }

    }
}
