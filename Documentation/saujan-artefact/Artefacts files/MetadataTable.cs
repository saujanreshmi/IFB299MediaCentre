﻿using System;
using System.Collections.Generic;
using MediaVault.Models;
using MySql.Data.MySqlClient;

namespace MediaVault.DatabaseLayer
{
	public static class MetadataTable
	{
		private static Metadata metadata = new Metadata();

		public static long AddMetadata(Metadata newmetadata)
		{
			long metadata_id = FillDatabase(newmetadata);

			return metadata_id;
		}

		public static Metadata GetMetadata(long user_id, long metadata_id)
		{
			SkimDatabase(user_id, metadata_id);
			return metadata;
		}

		/// <summary>
		/// Fills the table metadata in database mediavault 
		/// </summary>
		/// <returns>MID</returns>
		/// <param name="newmetadata">Metadata object</param>
		public static long FillDatabase(Metadata newmetadata)
		{
			string sql = @"INSERT INTO metadata(id, u_id, height, width, type, created_date, modified_date, author, album, genre, title) 
						   VALUES(@id, @u_id, @height, @width, @type, @created_date, @modified_date, @author, @album, @genre, @title)";
			DatabaseClass dlink = new DatabaseClass();
			MySqlCommand cmd = dlink.conn.CreateCommand();
			cmd.CommandText = sql;
			if (newmetadata != null)
			{
				if (newmetadata.UID < 0 && newmetadata.created_date == null && newmetadata.modified_date == null)
				{
					return 0;
				}
				else
				{
					cmd.Parameters.AddWithValue("@id", newmetadata.MID);
					cmd.Parameters.AddWithValue("@u_id", newmetadata.UID);
					cmd.Parameters.AddWithValue("@height", newmetadata.dimension_height);
					cmd.Parameters.AddWithValue("@width", newmetadata.dimension_width);
					cmd.Parameters.AddWithValue("@type", newmetadata.type);
					cmd.Parameters.AddWithValue("@created_date", newmetadata.created_date);
					cmd.Parameters.AddWithValue("@modified_date", newmetadata.modified_date);
					cmd.Parameters.AddWithValue("@author", newmetadata.author);
					cmd.Parameters.AddWithValue("@album", newmetadata.album);
					cmd.Parameters.AddWithValue("@genre", newmetadata.genre);
					cmd.Parameters.AddWithValue("@title", newmetadata.title);
					if (cmd.ExecuteNonQuery() == 1)
					{
						return cmd.LastInsertedId;
					}
				}
			}
			return 0; //not successful
		}

		public static void SkimDatabase(long user_id, long metadata_id)
		{
			DatabaseClass dlink = new DatabaseClass();
			MySqlCommand cmd = dlink.conn.CreateCommand();
			cmd.CommandText = @"SELECT * FROM metadata WHERE id=" + metadata_id.ToString();
			cmd.Connection = dlink.conn;
			MySqlDataReader reader = cmd.ExecuteReader();

			while (reader.Read())
			{
				//string v = reader.GetString(10).ToString();
				Dictionary<string, string> m = new Dictionary<string, string>();

				for (int i = 0; i < Metadata.Column; i++)
				{
					string value = reader.IsDBNull(i) ? "" : reader.GetString(i);
					m.Add(Metadata.Attributes[i], value);
				}
				//add in photo dictionary
				metadata = CreateMetadataObject(m);
			}
		}


		public static Metadata CreateMetadataObject(Dictionary<string, string> dbmetadata)
		{
			Metadata m = new Metadata();
			foreach (var item in dbmetadata)
			{
				switch (item.Key)
				{
					case "MID":
						m.MID = long.Parse(item.Value);
						break;
					case "UID":
						m.UID = long.Parse(item.Value);
						break;
					case "dimension_height":
						m.dimension_height = long.Parse(item.Value);
						break;
					case "dimension_width":
						m.dimension_width = long.Parse(item.Value);
						break;
					case "type":
						m.type = item.Value;
						break;
					case "created_date":
						m.created_date = DateTime.Parse(item.Value);
						break;
					case "modified_date":
						m.modified_date = DateTime.Parse(item.Value);
						break;
					case "author":
						m.author = item.Value;
						break;
					case "album":
						m.album = item.Value;
						break;
					case "genre":
						m.genre = item.Value;
						break;
					case "title":
						m.title = item.Value;
						break;
				}
			}
			return m;
		}
	}
}
