﻿using System;
using System.Web;
using System.Web.UI;

namespace MediaVault
{
	public partial class Default : System.Web.UI.Page
	{
		static SignIn s1;

		public void LoginClicked(object sender, EventArgs args)
		{
			s1 = new SignIn(usernametextbox.Text, passwordtextbox.Text);
			if (s1.feedback.Length != 0)
			{
				usernametextbox.Text = string.Empty;
				signinmessage.Text = s1.feedback;
			}
			else
			{
				signinmessage.Text = "Login Successful";
				Response.Redirect("Photo.aspx");
			}
		}

	public static long GetUseridLoggedInUser()
	{
		if (s1 != null)
		{
			return s1.userid;
		}
		return 0;
	}	}
}
