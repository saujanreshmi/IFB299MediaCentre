﻿using System.ComponentModel.DataAnnotations;
using System;

namespace MediaVault.Models
{
    public class Audio
    {
		public Audio()
		{
		}

		public const int Column = 9;

		public static string[] Attributes = new string[9] { "AID", "UID", "MID", "audio_name", "size", "audio_path","duration","samplerate","channels"};

        [ScaffoldColumn(false)]
        public long AID { get; set; }

        public long UID { get; set; }

        public long MID { get; set; }

        [Required, StringLength(50), Display(Name = "Name")]
        public string audio_name { get; set; }

        [ScaffoldColumn(false)]
        public long size { get; set; }

        [ScaffoldColumn(false)]
        public string audio_path { get; set; }

        [ScaffoldColumn(false)]
		public TimeSpan duration { get; set; }

        [ScaffoldColumn(false)]
        public long samplerate { get; set; }

        [ScaffoldColumn(false)]
        public long channels { get; set; }

        public virtual Metadata Metadata { get; set; }
    }
}
