﻿using System;
using MySql.Data.MySqlClient;

namespace MediaVault
{
	/// <summary>
	/// This class facilites the sigining up process for media vault.
	/// </summary>
	public class SignUp
	{
		private long id;
		private string username;
		private string password;
		private string email;
        public string feedback { get; }

		public SignUp(string email=null, string username=null, string password=null, string retypepassword=null)
		{
            this.feedback = "";
			if (email.Length == 0 || username.Length == 0 || password.Length == 0 || retypepassword.Length == 0)
			{
                this.feedback = "Email or Username or Password is empty";
			}

			else if (FieldExists(1, username))
			{
                this.feedback = "Username already exists";
			}

			else if (FieldExists(2, email))
			{

				this.feedback = "Email already exists";
			}

            else if (password != retypepassword)
			{
                this.feedback = "Password did not match";
			}
			this.id = 0;
			this.email = email;
			this.username = username;
			this.password = password;

		}

		private static bool FieldExists(int choice, string field)
		{
			string sql = "";
			switch (choice)
			{
				case 1:
					sql = @"SELECT name FROM user";
					break;
				case 2:
					sql = @"SELECT email FROM user";
					break;
			}
			DatabaseClass dlink = new DatabaseClass();
			MySqlCommand cmd = dlink.conn.CreateCommand();
			cmd.CommandText = (sql);
			cmd.Connection = dlink.conn;
			MySqlDataReader reader = cmd.ExecuteReader();
			while (reader.Read())
			{
				if (reader.GetString(0).ToString() == field)
				{
					cmd.Connection.Close();
					return true;
				}
			}
			cmd.Connection.Close();
			return false; //user doesnot exists
		}

		public bool FillDatbase()
		{
			string sql = @"INSERT INTO user(id, email, password, name) VALUES(@id,@email,@password,@name)";
			DatabaseClass dlink = new DatabaseClass();
			MySqlCommand cmd = dlink.conn.CreateCommand();
			cmd.CommandText = sql;
			cmd.Parameters.AddWithValue("@id", this.id);
			cmd.Parameters.AddWithValue("@email", this.email);
			cmd.Parameters.AddWithValue("@password", this.password);
			cmd.Parameters.AddWithValue("@name", this.username);
			if (cmd.ExecuteNonQuery() == 1)
			{
				return true;
			}
			return false;
		}
	}
}
