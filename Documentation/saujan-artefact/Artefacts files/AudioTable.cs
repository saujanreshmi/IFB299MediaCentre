﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace MediaVault.DatabaseLayer
{
	public class AudioTable
	{
		private Dictionary<long, Models.Audio> audios;

		public AudioTable()
		{
			this.audios = new Dictionary<long, Models.Audio>();
		}


		public Dictionary<long, Models.Audio> GetAudios(long user_id)
		{
			SkimDatabase(user_id);
			return this.audios;
		}


		public static long AddAudio(Models.Audio newaudio)
		{
			long audio_id = FillDatabase(newaudio);

			return audio_id;
		}


		private static long FillDatabase(Models.Audio newaudio)
		{
			string sql = @"INSERT INTO audio(id, u_id, m_id, name, size, location, duration, sample_rate, channels) 
                           VALUES(@id, @u_id, @m_id, @name, @size, @location, @duration, @sample_rate, @channels)";
			DatabaseClass dlink = new DatabaseClass();
			MySqlCommand cmd = dlink.conn.CreateCommand();
			cmd.CommandText = sql;
			if (newaudio != null)
			{
				if (newaudio.UID < 0 && newaudio.audio_name == null && newaudio.MID < 0 && newaudio.size < 0 && newaudio.audio_path == null 
				    && newaudio.duration.TotalSeconds < 0 && newaudio.Metadata == null)
				{
					return 0;
				}
				else
				{
					cmd.Parameters.AddWithValue("@id", newaudio.AID);
					cmd.Parameters.AddWithValue("@u_id", newaudio.UID);
					cmd.Parameters.AddWithValue("@m_id", newaudio.MID);
					cmd.Parameters.AddWithValue("@name", newaudio.audio_name);
					cmd.Parameters.AddWithValue("@size", newaudio.size);
					cmd.Parameters.AddWithValue("@location", newaudio.audio_path);
					cmd.Parameters.AddWithValue("@duration", newaudio.duration);
					cmd.Parameters.AddWithValue("@sample_rate", newaudio.samplerate);
					cmd.Parameters.AddWithValue("@channels", newaudio.channels);
					if (cmd.ExecuteNonQuery() == 1)
					{
						return cmd.LastInsertedId;
					}
				}
			}
			return 0; //not successf
		}

		public void SkimDatabase(long user_id)
		{
			DatabaseClass dlink = new DatabaseClass();
			MySqlCommand cmd = dlink.conn.CreateCommand();
			cmd.CommandText = @"SELECT * FROM audio WHERE u_id=" + user_id.ToString();
			cmd.Connection = dlink.conn;
			MySqlDataReader reader = cmd.ExecuteReader();
			while (reader.Read())
			{
				Dictionary<string, string> a = new Dictionary<string, string>();
				for (int i = 0; i < Models.Audio.Column; i++)
				{
					string value = reader.IsDBNull(i) ? "" : reader.GetString(i);
					a.Add(Models.Audio.Attributes[i], value);
				}
				//add in photo dictionary
				this.audios.Add(this.audios.Count+1, CreateAudioObject(a, user_id));

			}
		}

		private Models.Audio CreateAudioObject(Dictionary<string, string> dbphoto, long user_id)
		{
			Models.Audio a = new Models.Audio();
			foreach (var item in dbphoto)
			{
				switch (item.Key)
				{
					case "AID":
						a.AID = long.Parse(item.Value);
						break;
					case "UID":
						a.UID = long.Parse(item.Value);
						break;
					case "MID":
						a.MID =	long.Parse(item.Value);
						break;
					case "audio_name":
						a.audio_name = item.Value;
						break;
					case "size":
						a.size = long.Parse(item.Value);
						break;
					case "audio_path":
						a.audio_path = item.Value;
						break;
					case "duration":
						a.duration = TimeSpan.Parse(item.Value);
						break;
					case "samplerate":
						a.samplerate = long.Parse(item.Value);
						break;
					case "channels":
						a.channels = long.Parse(item.Value);
						break;
				}
			}
			a.Metadata = MetadataTable.GetMetadata(user_id, a.MID);
			return a;
		}
	}
}
