﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MediaVault
{

	public partial class Audio : System.Web.UI.Page
	{
		private DatabaseLayer.AudioTable au;
		private Dictionary<long, Models.Audio> a = new Dictionary<long, Models.Audio>();

		private void Page_Load(object sender, EventArgs e)
		{
			au = new DatabaseLayer.AudioTable();
			a = au.GetAudios(Default.GetUseridLoggedInUser());
			audioplayer.Visible = false;
			audiotrack.Visible = false;
			foreach (var item in a)
			{
				TableRow r = new TableRow();
				for (int i = 0; i < 6; i++)
				{
					TableCell c = new TableCell();
					switch (i)
					{
						case 0:
							ImageButton imgb = new ImageButton();
							imgb.ID = item.Key.ToString();
							imgb.ImageUrl = "~/Images/audioicon.png";
							imgb.Width = Unit.Pixel(20);
							imgb.Height = Unit.Pixel(20);
							imgb.OnClientClick = "ImageButtonClicked";
							imgb.Click += ImageButtonClicked;
							c.Controls.Add(imgb);
							c.HorizontalAlign = HorizontalAlign.Center;
							break;
						case 1:
							c.Text = item.Value.Metadata.title;
							break;
						case 2:
							c.Text = item.Value.duration.ToString();
							c.HorizontalAlign = HorizontalAlign.Center;
							break;
						case 3:
							c.Text = item.Value.Metadata.author;
							break;
						case 4:
							c.Text = item.Value.Metadata.album;
							break;
						case 5:
							c.Text = item.Value.Metadata.genre;
							break;
					}
					c.Attributes.Add("style","border-bottom: 1px solid LightGray;");
					r.Cells.Add(c);
				}
				audiotable.Rows.Add(r);
			}
		}

		public void ImageButtonClicked(object sender, EventArgs args)
		{
			string id = ((ImageButton)sender).ID;
			long songkey = long.Parse(id);
			audiotrack.InnerText = a[songkey].Metadata.title+" - "+a[songkey].Metadata.author + " ["+a[songkey].Metadata.album+"]";
			audioplayer.Attributes.Add("src", a[songkey].audio_path);
			audioplayer.EnableTheming = true;
			audioplayer.Visible = true;
			audiotrack.Visible = true;
			audioplayer.Attributes.Add("autoplay", "autoplay");
		}

		public void AudioButtonClicked(object sender, EventArgs args)
		{
			
		}
	}
}
